<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FormLoadTest extends DuskTestCase
{
    /**
    * A Dusk test example.
    *
    * @return void
    */
    use DatabaseMigrations;

    public function testFormRendering()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/submit-form')
            ->assertSee('Job Submit Form');
            ;
        });
    }

    public function testFormSubmissionSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('form')
            ->type('@job-title-input', "Test Job Title")
            ->type('@job-description-textarea', "Test Job Description")
            ->type('@job-email-input', "simon@clearsquares.com")
            ->press('@job-submit-button')
            ->pause('5000')
            ->assertVisible('@success-message-div')
            ->assertSeeIn('@success-message-div', 'Message Sent!')
            ;
        });
    }

    public function testTitleValidation()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('form')
            ->type('@job-description-textarea', "Test Job Description")
            ->type('@job-email-input', "simon@clearsquares.com")
            ->press('@job-submit-button')
            ->pause(1000)
            ->assertSee('title field')
            ;
        });
    }

    public function testDescriptionValidation()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('form')
            ->type('@job-title-input', "Test Job Title")
            ->type('@job-email-input', "simon@clearsquares.com")
            ->press('@job-submit-button')
            ->pause(1000)
            ->assertSee('description field')
            ;
        });
    }

    public function testEmailValidation()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('form')
            ->type('@job-title-input', "Test Job Title")
            ->type('@job-description-textarea', "Test Job Description")
            ->press('@job-submit-button')
            ->pause(1000)
            ->assertSee('email field')
            ;
        });
    }
}
