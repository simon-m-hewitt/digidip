<?php

namespace App\Mail;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class JobApprove extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $job;
    public $moderator;

    public function __construct(Job $job, $moderator)
    {
        $approve_path = env('MAIL_APPROVAL_PATH');
        $spam_path = env('MAIL_SPAM_PATH');
        $job->approve_link = url($approve_path) . "/$job->id";
        $job->spam_link = url($spam_path) . "/$job->id";
        $this->job = $job;
        $this->moderator = $moderator;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('jobs@theboard.com')
        ->view('emails/job_approval')
        ->text('emails/job_approval_plain');
    }
}
