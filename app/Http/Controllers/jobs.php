<?php

namespace App\Http\Controllers;

use App\Mail\JobApprove;
use App\Mail\JobModeration;
use App\Models\Job;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class jobs extends Controller
{
    public function submit_form()
    {
        Log::debug('Submit Form Loaded!');
        return view('submit_form');
    }

    public function submit(Request $request)
    {
        Log::debug("Hit the Submit Controller - Validating Request");
        $this->validate($request, [
            'title' => 'required|string',
            'description' => 'required|string',
            'email' => 'required|email',
        ]);
        Log::debug("Validation Complete");

        $exists = false;

        Log::debug("Creating Job Object");
        $job = new Job();
        $job->title = $request->title;
        $job->description = $request->description;
        $job->email = $request->email;

        Log::debug("Checking to see if the email already exists in the DB");
        $exists = $this->checkExists($job->email);

        if (!$exists) {
            Log::debug("Email didn't exist - setting as not approved");
            $job->approved = 0;
        } else {
            Log::debug("Email existed, setting as pre-approved");
            $job->approved = 1;
        }

        try {
            Log::debug("Saving Job ... ");
            $save = $job->save();
        } catch (Exception $ex) {
            Log::error("Error Saving Job To Database: $ex");
            $save = false;
        }
        if ($save) {
            Log::debug("Job Saved!");
            if (!$exists) {
                try {
                    Log::debug("Email didn't exist, trying to send email message ... ");
                    $mail = Mail::to($job->email)->send(new JobModeration($job));
                    $moderators = explode(',', env('MODERATORS'));
                    Log::debug("Moderators: " . json_encode($moderators));
                    foreach ($moderators as $moderator) {
                        Log::debug("Sending Approval Email To Moderator: $moderator");
                        Mail::to($moderator)->send(new JobApprove($job, $moderator));
                    }
                } catch (Exception $ex) {
                    Log::error("Error Sending Email To $job->email: $ex");
                    $mail = false;
                }
                if ($mail) {
                    Log::info("Email Sent To $job->email");
                } else {
                    Log::error("There was a problem sending the email to $job->email");
                }
            }
            Log::debug("OK, done!  Yay!");
            return response()->json(null, 200);
        } else {
            return response()->json(null, 500);
        }
    }

    private function checkExists($email)
    {
        try {
            $job = Job::get()->where('email', $email);
        } catch (Exception $ex) {
            Log::error("Database Error: $ex");
            $job = false;
        }
        if ($job) {
            if ($job->count() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
