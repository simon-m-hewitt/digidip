<div>

    <p>Hello <strong>{{ $job->email }}</strong>,</p>

    <p>Thanks for your submission.</p>

    <p>As this is the first time you have made a submission, it is in moderation for the time being.</p>

    <p>Once approved, it will be published.</p>

    <p>Kind Regards,</p>

    <p>The Job Board</p>

</div>
