Hello {{ $moderator }},

There has been a submission from a new email: {{ $job->email }}

Please check the submission below, and use the link to approve it, or mark it as spam;

Job Title: {{ $job->title }}
Description: {{ $job->description }}

Approve: {{ $job->approve_link }}
Mark As Spam: {{ $job->spam_link }}

Kind Regards,

The Job Board
