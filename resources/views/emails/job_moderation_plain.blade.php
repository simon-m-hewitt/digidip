Hello {{ $job->email }},

Thanks for your submission.

As this is the first time you have made a submission, it is in moderation for the time being.

Once approved, it will be published.

Kind Regards,

The Job Board
