<div>

    <p>Hello {{ $moderator }},</p>

    <p>There has been a submission from a new email: {{ $job->email }}</p>

    <p>Please check the submission below, and use the link to approve it, or mark it as spam;</p>

    <label for="title">Title: </label>
    <p id="title">{{ $job->title }}</p>

    <label for="description">Description: </label>
    <p id="description">{{ $job->description }}</p>

    <hr>

    <label for="approve">Approve: </label>
    <p id="approve">{{ $job->approve_link }}</p>

    <label for='spam'>Mark As Spam: </label>
    <p id="spam">{{ $job->spam_link }}</p>

    <p>Kind Regards,</p>

    <p>The Job Board</p>

</div>
